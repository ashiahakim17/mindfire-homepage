using System;

class Base
{
    public virtual void Print()
    {
	
	Console.WriteLine("Base class method is called");
    }
}

class Child1 : Base
{
    public override void Print()
    {
	
	Console.WriteLine("Child1 method is called");
    }
}

class Child2 : Base
{
    new public  void Print()
	{
	Console.WriteLine("Child2 method is called");
    }
}

class Program
{
    static void Main()
    {
		Base b=new Base();
		b.Print();
		Base b1=new Child1();
		b1.Print();
		Child2 c2=new Child2();
		c2.Print();
		Base b2=new Child2();
		b2.Print();

	
    }
}