//abstract class
using System;
abstract class abs
{
	public abstract void check(int a);
	
}
class child1:abs
{
	 public override  void  check(int a)
	{
		int sq=a*a;
		Console.WriteLine("child1 is called {0}",sq);
	}
}
class child2:abs
{
	public override  void check(int a)
	{
		int cube=a*a*a;
		Console.WriteLine("child2 is called {0}",cube);
	}
}
class program
{
	public static void Main()
	{
		Console.WriteLine("Enter the value:");
		int a=Convert.ToInt32(Console.ReadLine());
		abs ab1=new child1();
		ab1.check(a);
		//abs ab2=new child2();
		ab2.check(a);
		
	}
}