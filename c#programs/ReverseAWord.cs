using System;
class ReverseAWord{
	public static void Main(){
		int i=0;
		Console.WriteLine("Enter a string: ");
		string str=Console.ReadLine().Trim();
		char[] ch=str.ToCharArray();
		string[] temp=new string[1000];
		int j=0;
		for(i=0;i<ch.Length;i++)
		{
			if(((ch[i]<='Z')&&(ch[i]>='A'))||((ch[i]<='z')&&(ch[i]>='a'))||((ch[i]<='9')&&(ch[i]>='0')))
			{
				temp[j]=ch[i]+temp[j];
			}
			else
			{
				j++;
				temp[j]=ch[i].ToString();
				j++;
			}
		}
		for(i=0;i<temp.Length;i++)
		{
			Console.Write(temp[i]);
		}
	}
}