using System;

	namespace Interface
	{
		public interface IMessage
		
		{
		string Send();
		void Receive(string ReceivedMessage);
		}
	}
	
	