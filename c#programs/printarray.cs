using System;
using System.Linq;
using System.Text;
using System.IO;
public class Program
{
	public static void Main(string[] args)
	{
		Console.WriteLine("Enter the no of rows");
		int r=Convert.ToInt32(Console.ReadLine());
		int c=Convert.ToInt32(Console.ReadLine());
	    int[,] ar=new int[r,c];
		for(int i=0;i<r;i++)
		{
			for(int j=0;j<c;j++)
			{
				ar[i,j]=Convert.ToInt32(Console.ReadLine());
			}
		}
	
	
	    for(int i=0;i<r;i++)
		{
			for(int j=0;j<c;j++)
			{
				Console.Write("{0}\t",ar[i,j]);
			}
			Console.WriteLine("");
		}
	}
}