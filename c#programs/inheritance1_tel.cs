using System;
public class Telephone
{
	protected string phonetype;
	public virtual void ring()
	{
		Console.WriteLine(" Base class is called-Ringing the {0}",phonetype);
	}
	
}
public class electronic_phone:Telephone
{   
   
	 public electronic_phone(string str)
	{
		phonetype=str;
		
	}
	public override void ring()
	{
		Console.WriteLine("Child class is called-Ringing the {0}",phonetype);
	}
	public  void run()
	{
		ring();
	}
}
public class program
{
	public static void Main()
	{
		//electronic_phone ep=new electronic_phone("digital");
		Telephone t=new electronic_phone("manual");
		t.ring();
		electronic_phone e1=new electronic_phone("Digital");
		e1.run();
		//ep.run();
	
	}
}