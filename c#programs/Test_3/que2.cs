/*You are given an array of integers (both positive and negative). Find the continuous sequence with the largest sum. Return the sum.

example
Input: {2, -8, 3, -2, 4, -10}
Output: 5 (i.e., {3, -2, 4} )*/
using System;
public class Program
{
	public static void Main(string[] args)
	{

				  int currentSum = 0;
				  int currentMax = 0;
				  Console.WriteLine("Enter the length of the array:");
				  int n=Convert.ToInt32(Console.ReadLine());
				     int[] ar=new int[n];
				  Console.WriteLine("enter the array values:");
				
				  for(int i=0;i<n;i++)
				  {
					  ar[i]=Convert.ToInt32(Console.ReadLine());
				  }
				  for (int j=0; j <n; j++)
				  {
							currentSum += ar[ j ];

						   if (currentMax < currentSum) 
							    currentMax = currentSum;
						   else if (currentSum < 0)
							    currentSum = 0;
				  }
				 
				  Console.WriteLine("{0}",currentMax);
					 
	}
	
}