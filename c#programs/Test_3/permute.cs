using System.Collections.Generic;
using System;

using System.Linq;
using System.Text;

    class Program
    {
    static void Main(string[] args)
        {
			Console.WriteLine("Enter the string:");
			string a=Console.ReadLine();
            Permutation(a);
        }
   
    public static void Permutation(string input)
        {
            RecPermutation("", input);
        }
    private static void RecPermutation(string soFar, string input)
        {
    if (string.IsNullOrEmpty(input))
            {
                Console.WriteLine(soFar);
				return;
            }
    else
            {
    for (int i = 0; i < input.Length; i++)
                {
                    
					string remaining = input.Substring(0, i) + input.Substring(i + 1);
                    RecPermutation(soFar + input[i], remaining);
                }
            }
        }
    }
