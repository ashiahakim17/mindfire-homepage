
using System;
using System.Collections;

class Program
{
    static void Main()
    {
	Hashtable hashtable = new Hashtable();
	hashtable[1] = "One";
	hashtable[2] = "Two";
	hashtable[13] = "Thirteen";

	foreach (DictionaryEntry entry in hashtable)
	{
	    Console.WriteLine("{0}, {1}", entry.Key, entry.Value);
	}
    }
}