using System;

class Program
{
    struct Simple
    {
	public string name;
	public string city;
	public long phn;
	public double per;
    };

    static void Main()
    {
	Console.WriteLine("Enter the name");
	Simple s;
	s.name = Console.ReadLine();
	Console.WriteLine("Enter the city");
	s.city = Console.ReadLine();
	s.phn = 8280026757;
	Console.WriteLine("Enter the Percentage");
	s.per=Convert.ToDouble(Console.ReadLine());
	
    Console.WriteLine("Name: {0} City {1} Phone {2} Percentage {3}",s.name,s.city,s.phn,s.per);
    }
}