/*Create a method which accepts a String and replaces all the consonants in the String with the next alphabet.
Note: Consonant refers to all alphabets excluding vowels
Method Name
alterString
Method Description
Replace consonants
Argument
String
Return Type
String
Logic
Return the String replacing all the consonants with the next character.
For Example :JAVA should be changed as KAWA*/
using System;
public class replace
{
	
	public char[] alterString(string n)
	{
		    int k,i;
		
			
			char[] t= new char[n.Length];
			char[] str=n.ToCharArray();
			for(i=0;i<n.Length;i++)
			{
				if(str[i]!='a'&&str[i]!='e'&&str[i]!='i'&&str[i]!='o'&&str[i]!='u')
				{
					 k=(int)str[i];
					 k++;
					 t[i]=Convert.ToChar(k);
					// Console.WriteLine("{0}",t[i]);
					 
					 
				}
				else
				{
					 t[i]=str[i];
					  //Console.WriteLine("{0}",t[i]);
				}
			}
			/*for(m=0;m<n.Length;m++)
			{
		      t[m].ToString();
			}*/
			
			return t;
		
		
		
		
	}
	
}
public class Program
{
	public static void Main(string[] args)
	{
		string a;
		Console.WriteLine("Enter the string:");
		a=Console.ReadLine();
		replace r =new replace();
		char[] res=r.alterString(a);
		for(int i=0;i<a.Length;i++)
		{
		Console.Write("{0}",res[i]);
		}
		
	}
}
