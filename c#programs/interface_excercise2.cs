/*Exercise 13-3. Extend the IConvertible interface by creating a new interface,

ICodeChecker. The new interface should implement one new method,

CodeCheckSyntax, which takes two strings: the string to check and the language to

use. The method should return a bool. Revise the ProgramHelper class from

Exercise 13-2 to use the new interface.*/
using System;

using System.Collections.Generic;

using System.Linq;

using System.Text;
interface IConvertible
{
void ConvertToCSharp(string a);	
void ConvertToVB(string b);
	}
	
interface ICodeChecker
{
void CodeCheckSyntax(string c,string d);
}

public   class Convert:IConvertible,ICodeChecker
{

	
	public void CodeCheckSyntax(string c,string d)
	{
		Console.WriteLine("Your string is {0} and you have choosen {1}",c,d);
		
	}
	public void ConvertToCSharp(string a1)
			{
				Console.WriteLine("CSharp {0}",a1);
			}
	public void ConvertToVB(string a1)
	{
				Console.WriteLine("VBasic {0}",a1);
	}
	
}

class Tester
{
	static void Main()
	{
		Convert c=new Convert();
		Console.WriteLine("Enter the string:");
		string n1=Console.ReadLine();
		Console.WriteLine("Enter the language:");
	    string n2=Console.ReadLine();
		c.CodeCheckSyntax(n1,n2);
		if(n2=="csharp")
		{
			c.ConvertToCSharp(n1);
		}
		else if(n2=="vbasic")
		{
			c.ConvertToVB(n1);
		}
		else
		{
			Console.WriteLine("Wrong Entry");
		}
		
	}
}