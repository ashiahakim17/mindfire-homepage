using System;
public abstract class Telephone
{
	protected string phonetype;
	public abstract void ring();
	
	
}
public class Digital_phone:Telephone
{   
   
	 public Digital_phone(string str)
	{
		phonetype=str;
		
	}
	public override void ring()
	{
		Console.WriteLine("Digital is called-Ringing the {0}",phonetype);
	}
	
}
public class Talking_phone:Telephone
{   
   
	 public Talking_phone(string str1)
	{
		phonetype=str1;
		
	}
	public override void ring()
	{
		Console.WriteLine("Talking is called-Ringing the {0}",phonetype);
	}
	
}
public class program
{
	public static void Main()
	{
		//electronic_phone ep=new electronic_phone("digital");
		Telephone t1=new Talking_phone("Talking");
		t1.ring();
		Telephone t2=new Digital_phone("Digital");
		t2.ring();
		
		//ep.run();
	
	}
}