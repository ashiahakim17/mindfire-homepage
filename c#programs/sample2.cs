/* Create a class with a method to find the difference between the sum of the squares and the square of the sum of the first n natural numbers.
Method Name
calculateDifference
Method Description
Calculate the difference
Argument
int n
Return Type
int - Sum
Logic
Find the difference between the sum of the squares of the first n natural numbers and the square of their sum.
For Example if n is 10,you have to find
(1^2+2^2+3^2+….9^2+10^2)-(1+2+3+4+5…+9+10)^2*/
using System;

public class diff
{
	
	public int calculateDifference(int n)
	{
		int sum1=0;
		int sum2=0;
	 for(int i=1;i<=n;i++)
	 {
		sum1=sum1+((i)*(i));
	 }	
	 for(int j=1;j<=n;j++)
	 {
		sum2=sum2+j;
	 }	
	 int res=sum1-(sum2*sum2);
	 return res;
     	 
		
		
	}
}

public class Program
{
	public static void Main(string[] args)
	{
	Console.WriteLine("Enter the range:");	
	 diff d= new diff();
	int a=Convert.ToInt32(Console.ReadLine());
	int res=d.calculateDifference(a);
	Console.WriteLine("The Value is {0}",res);
		
		
	}
	
}



