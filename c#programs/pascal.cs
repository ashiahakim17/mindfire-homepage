//pascal triangle
/*1				
1	1			
1	2	1		
1	3	3	1	
1	4	6	4	1 */

using System;
using System.Linq;
using System.Text;
using System.IO;
public class Program
{
	public static void Main(string[] args)
	{    Console.WriteLine("Enter the size of an array");
		int size=Convert.ToInt32(Console.ReadLine());
		int [,] ar=new int[size,size];
		Console.WriteLine("Enter the array elements");
		for(int i=0;i<size;i++)
		{
			for(int j=0;j<size;j++)
			{
				ar[i,j]=Convert.ToInt32(Console.ReadLine());
			}
		}
		
		
		for(int k=0;k<size;k++)
		{
			for(int m=0;m<size;m++)
			{
				if(m==k)
				{
					ar[k,m]=1;
				}
				if(m==0)
				{
					ar[k,m]=1;
				}
				
			}
		}
		for(int g=0;g<size;g++)
		{
			for(int h=0;h<=g;h++)
			{
				
				if(g!=h && h!=0)
				{
					ar[g,h]=ar[g-1,h]+ar[g-1,h-1];
				}
			}
		}
		
		for(int a=0;a<size;a++)
		{
			for(int b=0;b<=a;b++)
			{
				Console.Write("{0}",ar[a,b]);
			}
			Console.WriteLine("");
		}
		
		
	}
	
}