//10. There is an array with every element repeated twice except one. Find that element?

//Read more: http://javarevisited.blogspot.com/2015/06/top-20-array-interview-questions-and-answers.html#ixzz4Ee3PYfYd
using System.Collections.Generic;
using System;

using System.Linq;
using System.Text;

    class Program
    {
    static void Main(string[] args)
        {
		
			Console.WriteLine("Enter the length of array:");
			int n=Convert.ToInt32(Console.ReadLine());
			int[] a=new int[n];
			int k,i,j,l,count;
			Console.WriteLine("Enter the array1:");
			for(k=0;k<n;k++)
			{
				a[k]=Convert.ToInt32(Console.ReadLine());
			}
			Array.Sort(a);
			for(l=0;l<n;l++)
			{
				Console.WriteLine("{0}",a[l]);
			}
			for(i=0;i<n;i++)
			{
				count=0;
				for(j=0;j<n;j++)
				{
					if(a[i]==a[j])
					{
						count++;
					}
				}
				if(count!=2)
				{
					Console.WriteLine("no is {0}",a[i]);
				}
			}
			
		}
		
	}