//How to find all pairs on integer array whose sum is equal to given number? 

//Read more: http://javarevisited.blogspot.com/2015/06/top-20-array-interview-questions-and-answers.html#ixzz4EYVP2eN7
using System.Collections.Generic;
using System;

using System.Linq;
using System.Text;

    class Program
    {
    static void Main(string[] args)
        {
		
			Console.WriteLine("Enter the length of array:");
			int n=Convert.ToInt32(Console.ReadLine());
			int[] ar=new int[n];
			int k,i,j;
			Console.WriteLine("Enter the array:");
			for(k=0;k<n;k++)
			{
				ar[k]=Convert.ToInt32(Console.ReadLine());
			}
			Console.WriteLine("Enter the Value");
			int f=Convert.ToInt32(Console.ReadLine());
			for(i=0;i<n;i++)
			{
				//int first=ar[i];
				for(j=i+1;j<n;j++)
				{
					//int sec=ar[j];
					if((ar[i]+ar[j])==f)
					{
						Console.WriteLine("{0}  {1} ",ar[i],ar[j]);
					}
				}
			}
		
		
		
		}
		
		
	}