//4. How to find largest and smallest number in unsorted array? 

//Read more: http://javarevisited.blogspot.com/2015/06/top-20-array-interview-questions-and-answers.html#ixzz4EYKoIXOf

using System.Collections.Generic;
using System;

using System.Linq;
using System.Text;

    class Program
    {
    static void Main(string[] args)
        {
			Console.WriteLine("Enter the length of array:");
			int n=Convert.ToInt32(Console.ReadLine());
			int[] ar=new int[n];
			int k,i;
			Console.WriteLine("Enter the array:");
			for(k=0;k<n;k++)
			{
				ar[k]=Convert.ToInt32(Console.ReadLine());
			}
			int max=ar[0];
			int min=ar[0];
			for(i=0;i<n;i++)
			{
				//max=a[i];
				if(ar[i]>max)
				{
					max=ar[i];
				}
				if(ar[i]<min)
				{
					min=ar[i];
				}
			}
			Console.WriteLine("Max is {0}, Min is {1} ",max,min);
			
			
		}
		
	}