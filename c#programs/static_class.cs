using System;
class Program
{
int SumVals(params int[] vals)
{
int sum = 0;
foreach (int val in vals)
{
sum = sum+val;               /* when both classes will be static then we dont have to create object we can directly access the object*/
}
return sum;
}
static void Main(string[] args)
{
Program ob=new Program();

int sum = ob.SumVals(1,3,4,5,6,7,8,1);
Console.WriteLine("Summed Values = {0}",sum);
Console.ReadKey();
}
}