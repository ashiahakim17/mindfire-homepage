//Program for palindrome
using System;
public class Palindrome
{
	public string IsPalindrome(int b)
	{
		int temp,rev,rem,rev1;
		string ans;
		rev=0;
		if(b>=10&&b<=10000)
		{ 
	        temp=b;
			while(temp>0)
			{
			 rem=temp%10;
             rev=(rev*10)+rem;
			 temp=temp/10;
			}
			rev1=rev;
			if(b==rev1)
			{
				ans="Yes";
				return ans;
			}
			else
			{
			  ans="No";	
			  return ans;
			}
		}
		else{
			ans="Invalid";
			return ans;
		}
	}
}
public class Program
{
	public static void Main(string[] args)
	{
		int a;
		string res;
		Console.WriteLine("Enter the number");
		a=Convert.ToInt32(Console.ReadLine());
		Palindrome p = new Palindrome( );


		res=p.IsPalindrome(a);
		Console.WriteLine("answer is {0}",res);
		Console.ReadLine();
	}
}