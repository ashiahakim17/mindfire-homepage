/*Create a class with a method which can calculate the sum of first n natural numbers which are divisible by 3 or 5.
Method Name
calculateSum
Method Description
Calculate Sum
Argument
int n
Return Type
int-sum
Logic
Calculate the sum of first n natural numbers which are divisible by 3 or 5.*/
using System;
public class divby
{
	
	public int CalculateSum(int n)
	{
		int sum=0;
		for(int i=1;i<=n;i++)
		{
			if(i%3==0 || i%5==0)
			{
				Console.WriteLine("{0}",i);
				sum=sum+i;
			}
			
		}
		return sum;
		
		
	}
}
public class Program
{
	public static void Main(string[] args)
	{
	Console.WriteLine("Enter the range:");
    int a=Convert.ToInt32(Console.ReadLine());	
	divby d=new divby();
	int res=d.CalculateSum(a);
	Console.WriteLine("Sum is {0}",res);	
		
	}
}


