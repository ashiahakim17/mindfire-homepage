/*Example 5: Create a method to check if a number is a power of two or not
Method Name
checkNumber
Method Description
Checks if the entered number is a power of two or not
Argument
int n
Return Type
boolean
Logic
Check if the input is a power of two.
Ex: 8 is a power of 2*/
using System;
public class powero
{
	
	public bool checkNumber(int n)
	{
		bool b=false;
		int temp=n;
		while(temp>1)
		{
			if(temp%2==0)
			{
				temp=temp/2;
				b=true;
			}
			else
			{
			   b=false;
               break;			   
			}
		}
		return b;
		
	}
}
public class Program
{
	public static void Main(string[] args)
	{
		Console.WriteLine("Enter the no");
		int a=Convert.ToInt32(Console.ReadLine());
		 powero p=new powero();
		 bool b1=p.checkNumber(a);
		 Console.WriteLine("{0}",b1);
		
	}
}

