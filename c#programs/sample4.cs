/*
Create a method to check if a number is an increasing number
Method Name
checkNumber
Method Description
Check if a number is an increasing number
Argument
int number
Return Type
boolean
Logic
A number is said to be an increasing number if no digit is exceeded by the digit to its left.
For Example : 134468 is an increasing number*/
using System;
public class chec
{
	
	public bool checkNumber(int n)
	{
		bool b=false;
		string a=n.ToString();
		for(int i=0;i<a.Length-1;i++)
		{
			if(a[i]<=a[i+1])
			{
				b=true;
			}
			else
			{
				b=false;
				break;
			}
		}
		return b;
		
	}
}

public class Program
{
	public static void Main(string[] args)
	{
		int a1;
		Console.WriteLine("Enter the no");
		a1=Convert.ToInt32(Console.ReadLine());
		chec c=new chec();
		bool a=c.checkNumber(a1);
		Console.WriteLine("ans is {0}",a);
		
	}
}
