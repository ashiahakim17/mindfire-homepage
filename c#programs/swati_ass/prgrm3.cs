//Program 3: Write a program to Find the frequency of the word “and” [if exists] in a given input string and replace all "and" with "AND".
using System.Collections.Generic;
using System;

using System.Linq;
using System.Text;

    class Program
    {
    static void Main(string[] args)
        {
		
			Console.WriteLine("Enter the string");
			string str=Console.ReadLine();
			string[] words = str.Split(' ');
			int count=0;
			string cmp="";
			foreach(string s in words)
			{
				if(s.Contains("and")==true)
				{
					
					count++;
					//var arr = s.ToCharArray();
					string rep=s.Replace("and","AND");
					
					cmp=cmp+" "+rep;
				}
				else
				{
					cmp=cmp+" "+s;
				}
			}
			Console.WriteLine("{0},{1}",cmp,count);
			
		
		
		
		}
		
	}