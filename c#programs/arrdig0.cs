//dig 0 lower -1 upper 1
using System;
using System.Linq;
using System.Text;
using System.IO;
public class Program
{
	public static void Main(string[] args)
	{
		Console.WriteLine("Enter the no of rows");
		int r=Convert.ToInt32(Console.ReadLine());
		int c=Convert.ToInt32(Console.ReadLine());
	    int[,] ar=new int[r,c];
		for(int i=0;i<r;i++)
		{
			for(int j=0;j<c;j++)
			{
				ar[i,j]=Convert.ToInt32(Console.ReadLine());
			}
		}
		for(int k=0;k<r;k++)
		{
			for(int l=0;l<c;l++)
			{
				if(k>l)
				{
					ar[k,l]=-1;
				}
				
				if(k<l)
				{
					ar[k,l]=1;
				}
				
				if(k==l)
				{
					ar[k,l]=0;
				}
				
			}
		}
		
		for(int m=0;m<r;m++)
		{
			for(int n=0;n<c;n++)
			{
				Console.Write("{0}\t",ar[m,n]);
			}
			Console.WriteLine("");
		}
		
	
	
	
	}
}