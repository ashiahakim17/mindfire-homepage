//string sorting and anagram
using System;

class Program
{
    static void Main()
    {
		int i;
		Console.WriteLine("enter the first string");
		string a=Console.ReadLine();
		Console.WriteLine("enter the second string");
		string b=Console.ReadLine();
	string a1=StringTools.Alphabetize(a);
	string b1=StringTools.Alphabetize(b);
	Console.WriteLine("Sorted String1: {0}",a1);
	Console.WriteLine("Sorted String2: {0}",b1);
	if(a1.Length==b1.Length)
	{
		for(i=0;i<a1.Length;i++)
		{
			if(a1[i]!=b1[i])
			{
				break;
			}
		}
		if(i==a1.Length)
		{
			Console.WriteLine("Anagram");
		}
		else
		{
			Console.WriteLine("Not Anagram");
		}
	}
	else
	{
	Console.WriteLine("Not Anagram");	
	}
    }
}

public static class StringTools
{
  
    public static string Alphabetize(string s)
    {
	char[] a = s.ToCharArray();

	
	Array.Sort(a);

	
	return new string(a);
    }
}