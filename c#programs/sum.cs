//Program for sum
using System;
public class Sum
{
	public int FindSum(int[] b,int cond ) 
	{
		int j,l1,c;
		c=0;
		l1=b.Length;
		int sum=0;
		for(j=0;j<l1;j++)
		{
			if(b[j]%cond==0)
			{
				sum=sum+b[j];
				c=c+1;
			}
		}
		if(c>0)
		{
		return sum;
		}
		else
		{
		  return -1;	
		}
		
	}
}
public class Program
{
	public static void Main(string[] args)
	{
		Console.WriteLine("Enter the no of values");
		int l=Convert.ToInt32(Console.ReadLine());
		int[] ar = new int[l];
		Sum s= new Sum();
		Console.WriteLine("Enter the values");
		for(int i=0;i<l;i++)
		{
			ar[i]=Convert.ToInt32(Console.ReadLine());
		}
		Console.WriteLine("Enter the condition");
		int c=Convert.ToInt32(Console.ReadLine());
		int res=s.FindSum(ar,c);
		Console.WriteLine("sum is {0}",res);
		Console.ReadLine();
		
	}
}