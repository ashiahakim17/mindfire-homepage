/*Create a class containing a method to create the mirror image of a String. The method should return the two Strings separated with a pipe(|) symbol .
Method Name
getImage
Method Description
Generate the mirror image of a String and add it to the existing string.
Argument
String
Return Type
String
Logic
Accepts One String
Find the mirror image of the String
Add the two Strings together separated by a pipe(|) symbol.
For Example
Input : EARTH
Output : EARTH|HTRAE
Hint: Use StringBuffer API (Ex: For this problem reverse method in Stringbuffer can be used)
Note: Learn the other APIs in StringBuffer*/
using System;

public class mirror1
{
	
	public string reverse(string n)
	{
		string str1="";
		for(int i=n.Length-1;i>=0;i--)
		{
			str1=str1+n[i];
		}
		return str1;
		
	}
	
}
public class Program
{
	public static void Main(string[] args)
	{
		string str;
		Console.WriteLine("Enter the string");
		str=Console.ReadLine();
		mirror1 m=new mirror1();
		string re=m.reverse(str);
		Console.WriteLine("{0}||{1}",str,re);
		
		
	}
	
}