/*Exercise 13-1. Define an interface IConvertible that indicates that the class can con-
vert a block of code to C# or VB. The interface should have two methods:

ConvertToCSharp and ConvertToVB. Each method should take a string and return a*/


using System;

using System.Collections.Generic;

using System.Linq;

using System.Text;



interface IConvertible
{
void ConvertToCSharp(string a);	
void ConvertToVB(string b);
	
}
public class Convert:IConvertible
{
	public void ConvertToCSharp(string a)
	{
		Console.WriteLine("csharp");
	}	
	public void  ConvertToVB(string b)
	{
		Console.WriteLine("Visual Basic");
	}
	
}
class Tester
{
	static void Main( )
	{
	  Convert c=new Convert();
       Console.WriteLine("Enter the string:");
       string str=Console.ReadLine();
       IConvertible ic=c as  IConvertible;
		ic.ConvertToCSharp(str);
		ic.ConvertToVB(str);
		
		
	}
}