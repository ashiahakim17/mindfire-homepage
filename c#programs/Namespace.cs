// Namespace Declaration
using System;

namespace namespace1
{
    // nested namespace
    namespace tutorial 
    {
        class Name 
        {
            public static void Print1() 
            {
                Console.WriteLine("My name is ashia.");
            }
        }
    }

  
    class NamespaceCalling 
    {
       
        public static void Main() 
        {
            
            tutorial.Name.Print1(); 
            tutorial.Address.Print2(); 
        }
    }
}


namespace namespace1.tutorial 
{
    class Address 
    {
        public static void Print2() 
        {
            Console.WriteLine("I am from Ranchi.");
        }
    }
}