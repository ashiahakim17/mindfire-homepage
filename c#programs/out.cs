//even no(add 2) odd no(minus 2)
using System;
using System.Linq;
using System.Text;
using System.IO;
class RefExample
{
    static void Method(out int i)
    {
		
        i=Convert.ToInt32(Console.ReadLine());
		
		if(i%2==0 )
		{
			Console.WriteLine("Even no");
			i=i+2;
			
		}
		else
		{
			Console.WriteLine("Odd no");
			i=i-2;
		
		}
    }

    static void Main()
    {
        
	    int a;
        Method(out a);
        Console.WriteLine("New value{0}",a);

       
    }
}