using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
//using ConsoleApplication1.GenericCollection;

namespace ConsoleApplication1
{
    
    public abstract class Vehicle
    {
        protected string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public Vehicle()
        {
            name = "The animal with no name";
        }

        public Vehicle(string newName)
        {
            name = newName;
        }

        public void Start()
        {
            Console.WriteLine("{0}'s Engine has been started.", Name);
        }
    }

    public class Train : Vehicle
    {
        public void Run()
        {
            Console.WriteLine("{0} has been started. running", Name);
        }

        public Train(string newName)
            : base(newName)
        {
        }
    }

    public class Aeroplance : Vehicle
    {
        public void Fly()
        {
            Console.WriteLine("{0} has started flying.", Name);
        }

        public Aeroplance(string newName)
            : base(newName)
        {
        }
    }

    public class ListBoxTest
    {
        private string[] strings;
        private int ctr = 0;
        // initialize the ListBox with strings
        public ListBoxTest(params string[] initialStrings)
        {
            // allocate space for the strings
            strings = new String[256];
            // copy the strings passed in to the constructor
            foreach (string s in initialStrings)
            {
                strings[ctr++] = s;
            }
        }

        // add a single string to the end of the ListBox
        public void Add(string theString)
        {
            if (ctr >= strings.Length)
            {
                // handle bad index
            }
            else
                strings[ctr++] = theString;
        }

        public string this[int index]
        {
            get
            {
                if (index < 0 || index >= strings.Length)
                {
                    // handle bad index
                }
                return strings[index];
            }
            set
            {
                // add new items only through the Add method
                if (index >= ctr)
                {
                    // handle error
                }
                else
                {
                    strings[index] = value;
                }
            }
        }

        // publish how many strings you hold
        public int GetNumEntries()
        {
            return ctr;
        }
    }

    public class ListBoxTest1 : IEnumerable<string>
    {
        private string[] strings;
        private int ctr = 0;
        // initialize the ListBox with strings
        public ListBoxTest1(params string[] initialStrings)
        {
            // allocate space for the strings
            strings = new String[256];
            // copy the strings passed in to the constructor
            foreach (string s in initialStrings)
            {
                strings[ctr++] = s;
            }
        }

        // add a single string to the end of the ListBox
        public void Add(string theString)
        {
            if (ctr >= strings.Length)
            {
                // handle bad index
            }
            else
                strings[ctr++] = theString;
        }

        public string this[int index]
        {
            get
            {
                if (index < 0 || index >= strings.Length)
                {
                    // handle bad index
                }
                return strings[index];
            }
            set
            {
                // add new items only through the Add method
                if (index >= ctr)
                {
                    // handle error
                }
                else
                {
                    strings[index] = value;
                }
            }
        }

        // publish how many strings you hold
        public int GetNumEntries()
        {
            return ctr;
        }

        public IEnumerator<string> GetEnumerator()
        {
            foreach (string s in strings)
            {
                yield return s;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }

    // a simple class to store in the List
    public class Employee2
    {
        private int empID;

        public Employee2(int empID) //constructor
        {
            this.empID = empID;
        }

        public override string ToString()
        {
            return empID.ToString();
        }
    }

    // a simple class to store in the List
    public class Employee1 : IComparable<Employee1>
    {
        private int empId;
        private int yearsOfSvc;

        public Employee1(int empID) //constructor
        {
            this.empId = empID;
        }

        public Employee1(int empId, int yearsOfSvc)
        {
            this.empId = empId;
            this.yearsOfSvc = yearsOfSvc;
        }

        public override string ToString()
        {
            return "Employee Id:" + empId.ToString() + "--Year of Service" + yearsOfSvc.ToString();
        }

        public int CompareTo(Employee1 other)
        {
            return this.empId.CompareTo(other.empId);
        }

        // Special implementation to be called by custom comparer
        public int CompareTo(Employee1 rhs, EmployeeComparer.ComparisonType which)
        {
            switch (which)
            {
                case EmployeeComparer.ComparisonType.EmpID:
                    return this.empId.CompareTo(rhs.empId);
                case EmployeeComparer.ComparisonType.YearsOfService:
                    return this.yearsOfSvc.CompareTo(rhs.yearsOfSvc);
            }
            return 0;
        }

        // static method to get a Comparer object
        public static EmployeeComparer GetComparer()
        {
            return new EmployeeComparer();
        }
    }

    // nested class which implements IComparer
    public class EmployeeComparer : IComparer<Employee1>
    {
        // private state variable
        private ComparisonType whichComparison;

        // enumeration of comparison types
        public enum ComparisonType
        {
            EmpID,
            YearsOfService
        };

        public bool Equals(Employee1 lhs, Employee1 rhs)
        {
            return this.Compare(lhs, rhs) == 0;
        }

        // Tell the Employee objects to compare themselves
        public int Compare(Employee1 lhs, Employee1 rhs)
        {
            return lhs.CompareTo(rhs, WhichComparison);
        }

        public ComparisonType WhichComparison
        {
            get { return whichComparison; }
            set { whichComparison = value; }
        }
    }

    public class MyGenericArray<T>
    {
        private T[] array;
        public MyGenericArray(int size)
        {
            array = new T[size + 1];
        }

        public T getItem(int index)
        {
            return array[index];
        }

        public void setItem(int index, T value)
        {
            array[index] = value;
        }
    }
    
    internal class Program
    {
        private static void Main(string[] args)
        {
            //ListWithForEachExample();

            //ArrayListExample();

            //EnumeratorExample();

            //ArrayListExample();

            //ListWithForEachExample();

            //GenericListWithSorting();

            // employee IDs

            //GenericListSortingByField();

            //QueueExample();

            //StackExample();

            // Create and initialize a new Dictionary.
            //DictionaryExample();

            //GenericClasses();

            //GenericMethods();

            //List<int> lstData = new List<int>();
            //lstData.Add(31);
            //lstData.Add(35);
            //lstData.Add(24);
            //lstData.Add(54);
            //lstData.Add(56);
            //lstData.Add(58);
            //lstData.Add(45);
            //lstData.Add(56);
            //lstData.Add(46);
            //lstData.Add(59);
            //lstData.Add(65);

            //IEnumerable<int> filteredData = GetData1(lstData);

            //foreach (var filterValue in filteredData)
            //{
            //    Console.WriteLine(filterValue);
            //}

            CustomCollectionExample();

            Console.ReadKey();
            Console.Read();
        }

        private static void CustomCollectionExample()
        {


            var person1 = new Person("Steve", "Jobs");
            var person2 = new Person("Bill", "Gates");
            var person3 = new Person("Ratan", "Tata");

            var businessObjList = new BusinessObjectCollection<Person>();
            businessObjList.Add(person1);
            businessObjList.Add(person2);
            businessObjList.Add(person3);

            Console.WriteLine("after adding to list..");
            Console.WriteLine("Total count {0}", businessObjList.Count);
            Console.WriteLine("----------------");
            foreach (var person in businessObjList)
            {
                Console.WriteLine(person.FirstName + " " + person.LastName);
            }

            Console.WriteLine();
            Console.WriteLine("after removing");
            businessObjList.Remove(person1);
            Console.WriteLine("Total count {0}", businessObjList.Count);
            Console.WriteLine("----------------");

            foreach (var person in businessObjList)
            {
                Console.WriteLine(person.FirstName + " " + person.LastName);
            }

            if (businessObjList.Contains(person2))
            {
                Console.WriteLine("Search matched");
                Console.WriteLine(businessObjList[0].FirstName + "--" + businessObjList[0].LastName);
            }
        }

        public static IEnumerable<int> GetData(List<int> dataList)
        {
            for (int index = 0; index < dataList.Count; index++)
            {
                var value = dataList[index];
                if (value > 50)
                    yield return value;
            }
        }

        public static IEnumerable<int> GetData1(List<int> dataList)
        {
            List<int> lstInt = new List<int>();

            for (int index = 0; index < dataList.Count; index++)
            {
                if (dataList[index] > 50)
                    lstInt.Add(dataList[index]);
            }

            return lstInt;
        }

        private static void GenericMethods()
        {
            int a, b;
            char c, d;
            a = 10;
            b = 20;
            c = 'I';
            d = 'V';

            //display values before swap:
            Console.WriteLine("Int values before calling swap:");
            Console.WriteLine("a = {0}, b = {1}", a, b);
            Console.WriteLine("Char values before calling swap:");
            Console.WriteLine("c = {0}, d = {1}", c, d);

            //call swap
            Swap<int>(ref a, ref b);
            Swap<char>(ref c, ref d);

            //display values after swap:
            Console.WriteLine("Int values after calling swap:");
            Console.WriteLine("a = {0}, b = {1}", a, b);
            Console.WriteLine("Char values after calling swap:");
            Console.WriteLine("c = {0}, d = {1}", c, d);
        }

        private static void GenericClasses()
        {
            //declaring an int array
            MyGenericArray<int> intArray = new MyGenericArray<int>(5);

            //setting values
            for (int c = 0; c < 5; c++)
            {
                intArray.setItem(c, c * 5);
            }

            //retrieving the values
            for (int c = 0; c < 5; c++)
            {
                Console.Write(intArray.getItem(c) + " ");
            }

            Console.WriteLine();

            //declaring a character array
            MyGenericArray<char> charArray = new MyGenericArray<char>(5);

            //setting values
            for (int c = 0; c < 5; c++)
            {
                charArray.setItem(c, (char)(c + 97));
            }

            //retrieving the values
            for (int c = 0; c < 5; c++)
            {
                Console.Write(charArray.getItem(c) + " ");
            }
            Console.WriteLine();
        }

        private static void DictionaryExample()
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();

            dict.Add("Bihar", "Patna");
            dict.Add("Odisha", "Bhubaneswar");
            dict.Add("Punjab", "Chandigarh");
            dict.Add("Sikim", "Gangtok");
            dict.Add("Rajstan", "Jaypur");

            Console.WriteLine("Capital of Odisha is {0}", dict["Odisha"]);

            foreach (var currVal in dict)
            {
                Console.WriteLine(" {0} --  {1}", currVal.Key, currVal.Value);
            }
        }

        private static void StackExample()
        {
            Stack<Int32> intStack = new Stack<Int32>();
            // populate the Stack
            for (int i = 0; i < 8; i++)
            {
                intStack.Push(i * 5);
            }
            // Display the Stack.
            Console.Write("intStack values:\t");
            PrintValues(intStack);
            // Remove an element from the Stack.
            Console.WriteLine("\n(Pop)\t{0}", intStack.Pop());
            // Display the Stack.
            Console.Write("intStack values:\t");
            PrintValues(intStack);
            // Remove another element from the Stack.
            Console.WriteLine("\n(Pop)\t{0}", intStack.Pop());
            // Display the Stack.
            Console.Write("intStack values:\t");
            PrintValues(intStack);
            // View the first element in the
            // Stack but do not remove.
            Console.WriteLine("\n(Peek) \t{0}", intStack.Peek());
        }

        private static void QueueExample()
        {
            //QUEUE
            Queue<Int32> intQueue = new Queue<Int32>();
            // populate the Queue.
            for (int i = 0; i < 5; i++)
            {
                intQueue.Enqueue(i * 5);
            }
            // Display the Queue.
            Console.Write("intQueue values:\t");
            PrintValues(intQueue);
            // Remove an element from the Queue.
            Console.WriteLine("\n(Dequeue)\t{0}", intQueue.Dequeue());
            // Display the Queue.
            Console.Write("intQueue values:\t");
            PrintValues(intQueue);
            // Remove another element from the Queue.
            Console.WriteLine("\n(Dequeue)\t{0}", intQueue.Dequeue());
            // Display the Queue.
            Console.Write("intQueue values:\t");
            PrintValues(intQueue);

            // View the first element in the
            // Queue but do not remove.
            Console.WriteLine("\n(Peek) \t{0}", intQueue.Peek());
            // Display the Queue.
            Console.Write("intQueue values:\t");
            PrintValues(intQueue);
        }

        private static void GenericListSortingByField()
        {
            var empList = new List<Employee1>();
            Random r = new Random();

            // populate the list
            for (int i = 0; i < 5; i++)
            {
                // add a random employee ID
                empList.Add(new Employee1(r.Next(10) + 100, r.Next(20)));
            }

            Console.WriteLine("Employee List");

            // display all the contents of the Employee list
            for (int i = 0; i < empList.Count; i++)
            {
                Console.Write(empList[i].ToString());
                Console.WriteLine();
            }

            Console.WriteLine("\n");

            Console.WriteLine("sorting employees by id");

            // sort and display the employee list
            EmployeeComparer c = Employee1.GetComparer();
            c.WhichComparison = EmployeeComparer.ComparisonType.EmpID;
            empList.Sort(c);

            // display all the contents of the Employee list
            for (int i = 0; i < empList.Count; i++)
            {
                Console.Write("\n{0} ", empList[i].ToString());
            }

            Console.WriteLine("\n");
            Console.WriteLine("sorting employees by year of service");
            Console.WriteLine();

            c.WhichComparison = EmployeeComparer.ComparisonType.YearsOfService;
            empList.Sort(c);
            for (int i = 0; i < empList.Count; i++)
            {
                Console.Write("\n{0} ", empList[i].ToString());
            }

            Console.WriteLine("\n");
        }

        private static void GenericListWithSorting()
        {
            var empList = new List<Employee1>();
            var intList = new List<int>();

            // populate the Lists
            for (var i = 0; i < 5; i++)
            {
                intList.Add(i * 5);
                empList.Add(new Employee1(i + 100,i+20));
            }

            Console.WriteLine("content of the int list");

            // print the contents of the int List
            for (int i = 0; i < intList.Count; i++)
            {
                Console.Write("{0} ", intList[i].ToString());
            }

            Console.WriteLine();
            Console.WriteLine("Content of the employee list");

            // print the contents of the Employee List
            for (int i = 0; i < empList.Count; i++)
            {
                Console.Write("{0} ", empList[i].ToString());
            }

            Console.WriteLine();
            Console.WriteLine("capacity of the employee list");
            Console.WriteLine(empList.Capacity);
            Console.WriteLine("\n");

            // sort and display the int list
            Console.WriteLine("List<int>after sorting:");
            intList.Sort();
            for (int i = 0; i < intList.Count; i++)
            {
                Console.Write("{0} ", intList[i].ToString());
            }

            Console.WriteLine("\n");
            // sort and display the Employee list
            Console.WriteLine("List<Employee>after sorting:");
            empList.Sort();

            // display all the contents of the Employee list
            for (int i = 0; i < empList.Count; i++)
            {
                Console.Write("{0} ", empList[i].ToString());
            }
            Console.WriteLine("\n");
        }

        private static void ListWithForEachExample()
        {

            // create a new ListBox and initialize
            ListBoxTest1 lbt = new ListBoxTest1("Hello", "World");

            // add a few strings
            lbt.Add("one");
            lbt.Add("two");
            lbt.Add("three");
            lbt.Add("four");
            string subst = "TWO";

            lbt[1] = subst;

            //// access all the strings
            //for (int i = 0; i < lbt.GetNumEntries(); i++)
            //{
            //    Console.WriteLine("lbt[{0}]: {1}", i, lbt[i]);
            //}

            foreach (var l in lbt)
            {
                if (l == null)
                    break;
                Console.WriteLine("value{0}", l.ToString());
            }
        }

        private static void ArrayListExample()
        {
            var vehicleArray = new Vehicle[2];
            var train = new Train("PuriHwdExpress");
            vehicleArray[0] = train;
            vehicleArray[1] = new Aeroplance("IndianAirlines");
            foreach (var t in vehicleArray)
            {
                Console.WriteLine("New {0} object added to Array collection ,Name {1}", t.ToString(),
                    t.Name);
                Console.WriteLine("Array collection contains {0} objects.", vehicleArray.Length);
            }

            vehicleArray[0].Start();
            ((Aeroplance)vehicleArray[1]).Fly();
            Console.WriteLine();
            Console.Read();

            /*ArrayList Implementation*/
            var vehArrayList = new ArrayList();

            vehArrayList.Add(new Train("Puri_Hwd Express"));
            vehArrayList.Add(new Aeroplance("IAirlines"));

            foreach (Train currTrain in vehArrayList)
            {
                Console.WriteLine("New {0} object added to Array collection ,Name {1}", currTrain.ToString(), currTrain.Name);
                Console.WriteLine("Array collection contains {0} objects.", vehicleArray.Length);
            }

            vehicleArray[0].Start();
            ((Aeroplance)vehicleArray[1]).Fly();
            Console.WriteLine();
            Console.ReadKey();
        }

        private static void EnumeratorExample()
        {
            string s = "Hello";
            // Because string implements IEnumerable, we can call GetEnumerator():
            IEnumerator rator = s.GetEnumerator();
            while (rator.MoveNext())
            {
                char c = (char)rator.Current;
                Console.Write(c + ".");
            }
        }

        static void Swap<T>(ref T lhs, ref T rhs)
        {
            T temp;
            temp = lhs;
            lhs = rhs;
            rhs = temp;
        }

        public static void PrintValues(IEnumerable<Int32> myCollection)
        {
            IEnumerator<Int32> myEnumerator = myCollection.GetEnumerator();
            while (myEnumerator.MoveNext())
            {
                Console.Write("{0} ", myEnumerator.Current);
            }

            Console.WriteLine();
        }
    }
}