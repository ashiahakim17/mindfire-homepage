using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

 public abstract class CustomerObjectBase
    {
	 protected Guid? _UniqueId;
        public CustomerObjectBase()
        {
            _UniqueId = Guid.NewGuid();
        }

        
        public Guid? UniqueId
        {
            get
            {
                return _UniqueId;
            }
            set
            {
                _UniqueId = value;
            }
        }
	
	}
	   public class Customer : CustomerObjectBase
    {
		private int _CustomerId;
        private string _FirstName = "";
        private string _LastName = "";
        private string _Age= "";
		private string _Address="";
		
		
		
        public Customer(int cid,string first,string last,string age,string add)
        {
			 _CustomerId=cid;
            _FirstName = first;
            _LastName = last;
			_Age=age;
			_Address=add;
			
        }
	

       
        public Customer()
        {
            
        }
		
		 public int CustomerId
        {
            get
            {
                return _CustomerId;
            }
            set
            {
                _CustomerId = value;
            }
        }

       
        public string FirstName
        {
            get
            {
                return _FirstName;
            }
            set
            {
                _FirstName = value;
            }
        }

       
        public string LastName
        {
            get
            {
                return _LastName;
            }
            set
            {
                _LastName = value;
            }
        }
		
		 public string Age
        {
            get
            {
                return _Age;
            }
            set
            {
                _Age = value;
            }
        }
		
		 public string Address
        {
            get
            {
                return _Address;
            }
            set
            {
                _Address = value;
            }
        }
		
		
   
		public  int CompareTo(Customer other)
    {
        // Comparing each item floating value using the following conditions
        return (CustomerId != other.CustomerId) ?
            ((CustomerId < other.CustomerId) ? 1 : -1) : 0;
    }
		
		/*public int CompareTo(object obj)

    {

        if (obj is Customer)

        {

            Customer c2 = (Customer)obj;

            return _FirstName.CompareTo(c2.FirstName);

        }

        //else

          //  throw new ArgumentException(“Object is not a Person.“);

    }*/
    }
	

class CustomerComparer : IComparer<Customer>
    {
        public int Compare(Customer x, Customer y)
        {
            // Invoking a CompareTo method to compare each 
            // item x of the collection with the current item y specified
            return x.CompareTo(y);
        }
    }
	
	
	 public class MyCustomerCollection<T> : ICollection<T> where T : CustomerObjectBase
    {
		 protected ArrayList _innerArray;
		 protected bool _IsReadOnly;
		 public MyCustomerCollection()
			{
				_innerArray = new ArrayList();
			}
		 public T this[int index]
		{
				get
				{	
					return (T)_innerArray[index];
				}
				set
				{
					_innerArray[index] = value;
				}
		}
		public virtual int Count
        {
            get
            {
                return _innerArray.Count;
            }
        }
		  public virtual bool IsReadOnly
        {
            get
            {
                return _IsReadOnly;
            }
        }
		  public virtual void Add(T CustomerObject)
        {
            _innerArray.Add(CustomerObject);
        }
		
		public void Sort()
    {
        // Invoking orderby LINQ aggregation method to sort
        // the items stored in the array m_Items
        _innerArray =  _innerArray.OrderBy<Customer, Customer>(customer =>CustomerObject ,
                new CustomerComparer()).ToArray();
    }
		
		
		
		 public virtual bool Remove(T CustomerObject)
        {
            bool result = false;

          
            for (int i = 0; i < _innerArray.Count; i++)
            {
             
                T obj = (T)_innerArray[i];

              
                if (obj.UniqueId == CustomerObject.UniqueId)
                {
                   
                    _innerArray.RemoveAt(i);
                    result = true;
                    break;
                }
            }

            return result;
        }
		
		

       
        public bool Contains(T CustomerObject)
        {
            
            foreach (T obj in _innerArray)
            {
              
                if (obj.UniqueId == CustomerObject.UniqueId)
                {
                    
                    return true;
                }
            }
            //no match
            return false;
        }

        
        public virtual void CopyTo(T[] CustomerObjectArray, int index)
        {
            throw new Exception(
              "This Method is not valid for this implementation.");
        }

       
        public virtual void Clear()
        {
            _innerArray.Clear();
        }
		 public virtual IEnumerator<T> GetEnumerator()
        {
            
            return new CustomerObjectEnumerator<T>(this);
        }

      
        IEnumerator IEnumerable.GetEnumerator()
        {
            return new CustomerObjectEnumerator<T>(this);
        }
		
		
		
	}
	
 
   
	 public class CustomerObjectEnumerator<T> : IEnumerator<T> where T : CustomerObjectBase
    {
        protected MyCustomerCollection<T> _collection;
        protected int index; 
        protected T _current; 
        // Default constructor
        public  CustomerObjectEnumerator()
        {
            //nothing
        }

        // Paramaterized constructor which takes
        // the collection which this enumerator will enumerate
        public CustomerObjectEnumerator(MyCustomerCollection<T> collection)
        {
            _collection = collection;
            index = -1;
            _current = default(T);
        }

        // Current Enumerated object in the inner collection
        public virtual T Current
        {
            get
            {
                return _current;
            }
        }

        // Explicit non-generic interface implementation for IEnumerator
        // (extended and required by IEnumerator<T>)
        object IEnumerator.Current
        {
            get
            {
                return _current;
            }
        }

        // Dispose method
        public virtual void Dispose()
        {
            _collection = null;
            _current = default(T);
            index = -1;
        }

        // Move to next element in the inner collection
        public virtual bool MoveNext()
        {
            //make sure we are within the bounds of the collection
            if (++index >= _collection.Count)
            {
                //if not return false
                return false;
            }
            else
            {
                //if we are, then set the current element
                //to the next object in the collection
                _current = _collection[index];
            }
            //return true
            return true;
        }

        // Reset the enumerator
        public virtual void Reset()
        {
            _current = default(T); //reset current object
            index = -1;
        }
    }
  internal class Program
    {
        private static void Main(string[] args)
        {
		 CustomCollectionExample2();

            Console.ReadKey();
            Console.Read();	
		}
	
	  private static void CustomCollectionExample2()
        {
		 Console.WriteLine("enter the no of records:");
         int n=Convert.ToInt32(Console.ReadLine());
		 string first,last,age,add;
		  int cid;
		 var CustomerObjList = new MyCustomerCollection<Customer>();
		 
		for(int i=1;i<=n;i++)
		{
			Console.WriteLine("Enter the detail {0}",i);
			Console.WriteLine("Enter the Customer Id");
			cid=Convert.ToInt32(Console.ReadLine());
			Console.WriteLine("Enter the First Name");
			first=Console.ReadLine();
			Console.WriteLine("Enter the Last Name");
			last=Console.ReadLine();
			Console.WriteLine("Enter the Age");
			age=Console.ReadLine();
			Console.WriteLine("Enter the Address");
			add=Console.ReadLine();
			 var cust = new Customer(cid,first,last,age,add);
			 CustomerObjList.Add(cust);
			 
			 
			
		}
		
		 CustomerObjList.Sort();
		
		  Console.WriteLine("after adding to list..");
            Console.WriteLine("Total count {0}", CustomerObjList.Count);
            Console.WriteLine("----------------");
            foreach (var customer in CustomerObjList)
            {
               Console.WriteLine(customer.CustomerId + " " + customer.FirstName + " " +customer.LastName  + " " + customer.Age + " " + customer.Address);
            }
			
			label1: string res=option();
	       switch(res)
		     {
			   case "1":
			   {
				 Console.WriteLine("Enter the customer id to be Deleted:");
			 string find=Console.ReadLine();
			
			 foreach (var customer in CustomerObjList)
            {
               if(customer.CustomerId==find)
			   {
				var del=customer;  
				 CustomerObjList.Remove(del);
			   }
			   else
			   {
				   Console.WriteLine("Record doesn't exist for this ID"); 
			   }
            }
			
			Console.WriteLine("Total count {0}", CustomerObjList.Count);
            Console.WriteLine("----------------");
			foreach (var customer in CustomerObjList)
            {
                Console.WriteLine(customer.CustomerId + " " + customer.FirstName + " " +customer.LastName  + " " + customer.Age + " " + customer.Address);
            }  
			   }
			    goto label1;
			  //break;
			case "2":
			{
				Console.WriteLine("Enter the CustomerId of the customer whose details has to be searched:");
			 string search=Console.ReadLine();
			
			 foreach (var customer in CustomerObjList)
            {
               if(customer.CustomerId==search)
			   {
				var sea=customer;  
				 CustomerObjList.Contains(sea);
				Console.WriteLine(customer.CustomerId + " " + customer.FirstName + " " +customer.LastName  + " " + customer.Age + " " + customer.Address);
			   }
			   else
			   {
				 Console.WriteLine("Record doesn't exist for this ID");  
				 
			   }
            }
			}
			goto label1;
			//break;
			case "3":
			{
			Console.WriteLine("Enter the no of records you want to add:");
			int n1=Convert.ToInt32(Console.ReadLine());
			
			for(int i=1;i<=n1;i++)
		{
			Console.WriteLine("Enter the detail {0}",i);
			Console.WriteLine("Enter the Customer Id");
			cid=Console.ReadLine();
			Console.WriteLine("Enter the First Name");
			first=Console.ReadLine();
			Console.WriteLine("Enter the Last Name");
			last=Console.ReadLine();
			Console.WriteLine("Enter the Age");
			age=Console.ReadLine();
			Console.WriteLine("Enter the Address");
			add=Console.ReadLine();
			 var cust = new Customer(cid,first,last,age,add);
			 CustomerObjList.Add(cust);
		}
		
		Console.WriteLine("Total count {0}", CustomerObjList.Count);
            Console.WriteLine("----------------");
			foreach (var customer in CustomerObjList)
            {
                Console.WriteLine(customer.CustomerId + " " + customer.FirstName + " " +customer.LastName  + " " + customer.Age + " " + customer.Address);
            }
			}
			goto label1;
			//break;
		
			case "4":
			Environment.Exit(0);
			 //EmployeeObjList.Sort();
             ////foreach (var customer in EmployeeObjList)
           // {
                //Console.WriteLine(customer.FirstName + " " + customer.LastName+ " " + customer.CustomerId + " " + customer.Age + " " + customer.Address);
           // }

       
			break;
			default:
			Console.WriteLine("Enter the valid Values");
			break;
		}
			
			
			
		
		
			
			
		}
		  static string option()
	  {
		  Console.WriteLine("1=>remove items");
		   Console.WriteLine("2=>Searching");
		    Console.WriteLine("3=>adding new entries");
			Console.WriteLine("4=>sorting");
			Console.WriteLine("5=>exit");
		  string op=Console.ReadLine();
		  return op;
	  }
	}
	