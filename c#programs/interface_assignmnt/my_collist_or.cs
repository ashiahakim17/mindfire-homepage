using System;
using System.Collections.Generic;

class Customer : IEquatable<Customer>,IComparable<Customer>
{
	public static int val=0;
    public int Cid { get; set; }
    public string First { get; set; }
	 public string Last { get; set; }
    public int Age { get; set; }
	 public string Add { get; set; }
   
   
	public override string ToString()
        {
            return "Cid: " + Cid + "   Name: " + First + " last: "+ Last +" Age:" +Age + " Add: " + Add;
        }
  
      public override bool Equals(object obj)
        {
            if (obj == null) return false;
            Customer objAsCustomer = obj as Customer;
            if (objAsCustomer == null) return false;
            else return Equals(objAsCustomer);
        }
        public override int GetHashCode()
        {
            return Cid;
        }
        public bool Equals(Customer other)
        {
            if (other == null) return false;
            return (this.Cid.Equals(other.Cid));
        }
		
		/*public int CompareTo(Customer compareCustomer)
    {
          // A null value means that this object is greater.
        if (compareCustomer == null)
            return 1;

        else
            return this.Cid.CompareTo(compareCustomer.Cid);
    }*/
	
	public int CompareTo(Customer compareCustomer)
    {
          // A null value means that this object is greater.
        if (compareCustomer == null)
            return 1;

        else
		{
			//Console.WriteLine("enter value:");
			//string a=Console.ReadLine();
			switch(val)
			{
				  case 1: return this.Cid.CompareTo(compareCustomer.Cid);
				        
                
				  
				  case 2: return this.First.CompareTo(compareCustomer.First);
				  
				    case 3: return this.Last.CompareTo(compareCustomer.Last);
				        
                  case 4: return this.Age.CompareTo(compareCustomer.Age);
			
			 
	       
			 default:
			 return this.Cid.CompareTo(compareCustomer.Cid);
			}
		}
	   
    }
	 
  
}

class Program
{
    public static void Main()
    {
	List<Customer> list = new List<Customer>();
	list=adding(list);
	/*foreach (var element in list)
	{
	    Console.WriteLine(element);
	}*/
	// Console.WriteLine("----------------------");
	showlist(list);
	// Console.WriteLine("----------------------");
	label1: string res=option();
	switch(res)
		{
			case "1":
			list=deleting(list);
			showlist(list);
			goto label1;
			//break;
			case "2":
			searching(list);
			goto label1;
			//break;
			case "3":
			list=adding(list);
			showlist(list);
			goto label1;
			//break;
			case "4":
			list.Sort();
			showlist(list);
			goto label1;
			//break;
			case "5":
			Environment.Exit(0);
			break;
			default:
			Console.WriteLine("Enter the valid Values");
			break;
		}
	
	
		
	
	
	
    }
	static List<Customer> adding(List<Customer> list1)
    {
	Console.WriteLine("enter the no of records:");
     int n=Convert.ToInt32(Console.ReadLine());
     string first,last,add;
	 int age,cid;
	//List<Customer> list1 = new List<Customer>();
		 
		for(int i=1;i<=n;i++)
		{
			Console.WriteLine("Enter the detail {0}",i);
			Console.WriteLine("Enter the Customer Id");
			cid=Convert.ToInt32(Console.ReadLine());
			Console.WriteLine("Enter the First Name");
			first=Console.ReadLine();
			Console.WriteLine("Enter the Last Name");
			last=Console.ReadLine();
			Console.WriteLine("Enter the Age");
			age=Convert.ToInt32(Console.ReadLine());
			Console.WriteLine("Enter the Address");
			add=Console.ReadLine();
			list1.Add(new Customer() { Cid = cid, First = first, Last=last, Age=age, Add=add });
		}
	
	
	return list1;

	// Uses IComparable.CompareTo()
	//list.Sort();

	// Uses Employee.ToString
	

     }
	 static List<Customer> deleting(List<Customer> list1)
	 {
		 Console.WriteLine("Enter the Customer Id");
   int del=Convert.ToInt32(Console.ReadLine());
    list1.Remove(new Customer(){Cid=del});
	  return list1;
		 
	 }
	 
	 static void showlist(List<Customer> list1)
	 {
		 Console.WriteLine("----------------------");
		foreach (var element in list1)
	   {
	    Console.WriteLine(element);
	   } 
	   Console.WriteLine("----------------------");
		 
	 }
	  static string option()
	  {
		  Console.WriteLine("1=>remove items");
		  Console.WriteLine("2=>Searching");
		    Console.WriteLine("3=>adding new entries");
			Console.WriteLine("4=>sorting");
			Console.WriteLine("5=>exit");
		  string op=Console.ReadLine();
		  return op;
	  }
	   static void searching(List<Customer> list1)
	 {
		 Console.WriteLine("Enter the Customer Id to search");
   int sea=Convert.ToInt32(Console.ReadLine());
		 if(list1.Contains(new Customer {Cid=sea}))
			 {
				 
				foreach (Customer cus in list1 )
				{
					if(cus.Cid==sea)
					{
					Console.WriteLine(cus);
					}
				} 
			 }
		 
	 }
	 
	 
	
	
}
