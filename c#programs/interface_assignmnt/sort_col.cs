using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
class Item : IComparable<Item>
{
    private float m_value; // private float data field
    public  Item(float value) { m_value = value; }
    public  float GetItem() { return m_value; }
    public  int CompareTo(Item other)
    {
        // Comparing each item floating value using the following conditions
        return (this.GetItem() != other.GetItem()) ?
            ((this.GetItem() < other.GetItem()) ? 1 : -1) : 0;
    }
}
class ItemsComparer : IComparer<Item>
    {
        public int Compare(Item x, Item y)
        {
            // Invoking a CompareTo method to compare each 
            // item x of the collection with the current item y specified
            return x.CompareTo(y);
        }
    }
	class MyItemsCollection : IEnumerable<Item>
{
    // The array of items of Item data type
    private static Item[] m_Items = null;
    // The array items index variable m_ItemIndex
    private static int m_ItemIndex = 0;
    public MyItemsCollection()
    {
        // Initializing the array of items
        if (m_Items == null)
            m_Items = new Item[0];
    }

    public void Add(Item item)
    {
        // Growing the size of the array
        Array.Resize<Item>(ref m_Items, m_ItemIndex + 1);
        // Assigning a value to the current item of m_Items array
        // accessed by its m_ItemIndex index
        m_Items[m_ItemIndex++] = item;
    }

    public void Sort()
    {
        // Invoking orderby LINQ aggregation method to sort
        // the items stored in the array m_Items
        m_Items = m_Items.OrderBy<Item, Item>(item => item,
                new ItemsComparer()).ToArray();
    }

    public IEnumerator GetEnumerator()
    {
        // Retriving the value of the generic enumerator
        // for the array m_Items
        return m_Items.GetEnumerator();
    }

    IEnumerator<Item> IEnumerable<Item>.GetEnumerator()
    {
        // Invoking the GetEnumerator() methods of this object
        // and assign its returning value to the returning value
        // of this function explicitly casting it to the type of IEnumerator<Item>
        return (IEnumerator<Item>)this.GetEnumerator();
    }
}
class Program
{
    static void Main(string[] args)
    {
        // Initializing the custom collection's object
        MyItemsCollection items = new MyItemsCollection();
        // Adding items to the collection of data
        items.Add(new Item(3.5F));
        items.Add(new Item(0.58F));
        items.Add(new Item(1.16F));
        items.Add(new Item(0.32F));
        items.Add(new Item(0.57F));

        // Sorting the collection of data
        items.Sort();

        // Iterating through the collection of data constructed
        // Retrieving items using GetItem() method and providing
        // the console output for each item fetched from the current collection
        foreach (Item item in items)
            Console.WriteLine("{0:F}", item.GetItem());

        Console.ReadKey();
    }
}