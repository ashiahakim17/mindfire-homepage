using System;
using System.Collections.Generic;

class Employee : IEquatable<Employee>,IComparable<Employee>
{
    public int Cid { get; set; }
    public string First { get; set; }
	 public string Last { get; set; }
    public int Age { get; set; }
	 public string Add { get; set; }
   
   
	public override string ToString()
        {
            return "Cid: " + Cid + "   Name: " + First + " last: "+ Last +" Age:" +Age + " Add: " + Add;
        }
   /* public int CompareTo(Employee other)
    {
	// Alphabetic sort if salary is equal. [A to Z]
	if (this.Salary == other.Salary)
	{
	    return this.Name.CompareTo(other.Name);
		: IComparable<Employee>
	}
	// Default to salary sort. [High to low]
	return other.Salary.CompareTo(this.Salary);
    }*/
      public override bool Equals(object obj)
        {
            if (obj == null) return false;
            Employee objAsPart = obj as Employee;
            if (objAsPart == null) return false;
            else return Equals(objAsPart);
        }
        public override int GetHashCode()
        {
            return Cid;
        }
        public bool Equals(Employee other)
        {
            if (other == null) return false;
            return (this.Cid.Equals(other.Cid));
        }
		
		public int CompareTo(Employee compareEmployee)
    {
          // A null value means that this object is greater.
        if (compareEmployee == null)
            return 1;

        else
            return this.Cid.CompareTo(compareEmployee.Cid);
    }
	 
  
}

class Program
{
    static void Main()
    {
	List<Employee> list = new List<Employee>();
	//adding();
	
	 Console.WriteLine("enter the no of records:");
     int n=Convert.ToInt32(Console.ReadLine());
     string first,last,add;
	 int age,cid;
	
		 
		for(int i=1;i<=n;i++)
		{
			Console.WriteLine("Enter the detail {0}",i);
			Console.WriteLine("Enter the Customer Id");
			cid=Convert.ToInt32(Console.ReadLine());
			Console.WriteLine("Enter the First Name");
			first=Console.ReadLine();
			Console.WriteLine("Enter the Last Name");
			last=Console.ReadLine();
			Console.WriteLine("Enter the Age");
			age=Convert.ToInt32(Console.ReadLine());
			Console.WriteLine("Enter the Address");
			add=Console.ReadLine();
			list.Add(new Employee() { Cid = cid, First = first, Last=last, Age=age, Add=add });
		}
	
	
	

	// Uses IComparable.CompareTo()
	//list.Sort();

	// Uses Employee.ToString
	foreach (var element in list)
	{
	    Console.WriteLine(element);
	}
	Console.WriteLine("Enter the Customer Id");
   int del=Convert.ToInt32(Console.ReadLine());
    list.Remove(new Employee(){Cid=del});
	
	

	foreach (Employee emp in list)
        {
            Console.WriteLine(emp);
        }
			Console.WriteLine("Enter the Customer Id to search");
   int sea=Convert.ToInt32(Console.ReadLine());
		 if(list.Contains(new Employee {Cid=sea}))
			 {
				 
				foreach (Employee emp in list )
				{
					if(emp.Cid==sea)
					{
					Console.WriteLine(emp);
					}
				} 
			 }
			
		Console.WriteLine("----------------------");	 
		list.Sort();
	    foreach (Employee emp2 in list)
        {
            Console.WriteLine(emp2);
        }
	
	
	
    }
	
}
