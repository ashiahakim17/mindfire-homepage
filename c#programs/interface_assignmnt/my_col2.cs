using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

 public abstract class EmployeeObjectBase
    {
	 protected Guid? _UniqueId;
        public EmployeeObjectBase()
        {
            _UniqueId = Guid.NewGuid();
        }

        
        public Guid? UniqueId
        {
            get
            {
                return _UniqueId;
            }
            set
            {
                _UniqueId = value;
            }
        }
	
	}
	   public class Customer : EmployeeObjectBase
    {
		private string _CustomerId="";
        private string _FirstName = "";
        private string _LastName = "";
        private string _Age= "";
		private string _Address="";
		
		
        public Customer(string cid,string first,string last,string age,string add)
        {
			 _CustomerId=cid;
            _FirstName = first;
            _LastName = last;
			_Age=age;
			_Address=add;
			
        }

       
        public Customer()
        {
            
        }
		
		 public string CustomerId
        {
            get
            {
                return _CustomerId;
            }
            set
            {
                _CustomerId = value;
            }
        }

       
        public string FirstName
        {
            get
            {
                return _FirstName;
            }
            set
            {
                _FirstName = value;
            }
        }

        //Person's Last Name
        public string LastName
        {
            get
            {
                return _LastName;
            }
            set
            {
                _LastName = value;
            }
        }
		
		 public string Age
        {
            get
            {
                return _Age;
            }
            set
            {
                _Age = value;
            }
        }
		
		 public string Address
        {
            get
            {
                return _Address;
            }
            set
            {
                _Address = value;
            }
        }
		
		/*public int CompareTo(object obj)

    {

        if (obj is Customer)

        {

            Customer c2 = (Customer)obj;

            return _FirstName.CompareTo(c2.FirstName);

        }

        //else

          //  throw new ArgumentException(“Object is not a Person.“);

    }*/
    }
	
	 public class MyEmployeeCollection<T> : ICollection<T> where T : EmployeeObjectBase
    {
		 protected ArrayList _innerArray;
		 protected bool _IsReadOnly;
		 public MyEmployeeCollection()
			{
				_innerArray = new ArrayList();
			}
		 public T this[int index]
		{
				get
				{	
					return (T)_innerArray[index];
				}
				set
				{
					_innerArray[index] = value;
				}
		}
		public virtual int Count
        {
            get
            {
                return _innerArray.Count;
            }
        }
		  public virtual bool IsReadOnly
        {
            get
            {
                return _IsReadOnly;
            }
        }
		  public virtual void Add(T EmployeeObject)
        {
            _innerArray.Add(EmployeeObject);
        }
		
		
		
		 public virtual bool Remove(T EmployeeObject)
        {
            bool result = false;

            //loop through the inner array's indices
            for (int i = 0; i < _innerArray.Count; i++)
            {
                //store current index being checked
                T obj = (T)_innerArray[i];

                //compare the BusinessObjectBase UniqueId property
                if (obj.UniqueId == EmployeeObject.UniqueId)
                {
                    //remove item from inner ArrayList at index i
                    _innerArray.RemoveAt(i);
                    result = true;
                    break;
                }
            }

            return result;
        }
		
		/*public virtual bool Remove(string f)
        {
            bool result = false;

            //loop through the inner array's indices
            for (int i = 0; i < _innerArray.Count; i++)
            {
                //store current index being checked
                T obj = (T)_innerArray[i];

                //compare the BusinessObjectBase UniqueId property
                if (obj.UniqueId == f.UniqueId)
                {
                    //remove item from inner ArrayList at index i
                    _innerArray.RemoveAt(i);
                    result = true;
                    break;
                }
            }

            return result;
        }*/

        // Returns true/false based on whether or not it finds
        // the requested object in the collection.
        public bool Contains(T EmployeeObject)
        {
            //loop through the inner ArrayList
            foreach (T obj in _innerArray)
            {
                //compare the BusinessObjectBase UniqueId property
                if (obj.UniqueId == EmployeeObject.UniqueId)
                {
                    //if it matches return true
                    return true;
                }
            }
            //no match
            return false;
        }

        // Copy objects from this collection into another array
        public virtual void CopyTo(T[] EmployeeObjectArray, int index)
        {
            throw new Exception(
              "This Method is not valid for this implementation.");
        }

        // Clear the collection of all it's elements
        public virtual void Clear()
        {
            _innerArray.Clear();
        }
		 public virtual IEnumerator<T> GetEnumerator()
        {
            
            return new EmployeeObjectEnumerator<T>(this);
        }

        // Explicit non-generic interface implementation for IEnumerable
        // extended and required by ICollection (implemented by ICollection<T>)
        IEnumerator IEnumerable.GetEnumerator()
        {
            return new EmployeeObjectEnumerator<T>(this);
        }
		
		
		
	}
	

   
	 public class EmployeeObjectEnumerator<T> : IEnumerator<T> where T : EmployeeObjectBase
    {
        protected MyEmployeeCollection<T> _collection;
        protected int index; 
        protected T _current; 
        // Default constructor
        public  EmployeeObjectEnumerator()
        {
            //nothing
        }

        // Paramaterized constructor which takes
        // the collection which this enumerator will enumerate
        public EmployeeObjectEnumerator(MyEmployeeCollection<T> collection)
        {
            _collection = collection;
            index = -1;
            _current = default(T);
        }

        // Current Enumerated object in the inner collection
        public virtual T Current
        {
            get
            {
                return _current;
            }
        }

        // Explicit non-generic interface implementation for IEnumerator
        // (extended and required by IEnumerator<T>)
        object IEnumerator.Current
        {
            get
            {
                return _current;
            }
        }

        // Dispose method
        public virtual void Dispose()
        {
            _collection = null;
            _current = default(T);
            index = -1;
        }

        // Move to next element in the inner collection
        public virtual bool MoveNext()
        {
            //make sure we are within the bounds of the collection
            if (++index >= _collection.Count)
            {
                //if not return false
                return false;
            }
            else
            {
                //if we are, then set the current element
                //to the next object in the collection
                _current = _collection[index];
            }
            //return true
            return true;
        }

        // Reset the enumerator
        public virtual void Reset()
        {
            _current = default(T); //reset current object
            index = -1;
        }
    }
  internal class Program
    {
        private static void Main(string[] args)
        {
		 CustomCollectionExample2();

            Console.ReadKey();
            Console.Read();	
		}
	
	  private static void CustomCollectionExample2()
        {
		 Console.WriteLine("enter the no of records:");
         int n=Convert.ToInt32(Console.ReadLine());
		 string cid,first,last,age,add;
		 var EmployeeObjList = new MyEmployeeCollection<Customer>();
		for(int i=1;i<=n;i++)
		{
			Console.WriteLine("Enter the detail {0}",i);
			Console.WriteLine("Enter the Customer Id");
			cid=Console.ReadLine();
			Console.WriteLine("Enter the First Name");
			first=Console.ReadLine();
			Console.WriteLine("Enter the Last Name");
			last=Console.ReadLine();
			Console.WriteLine("Enter the Age");
			age=Console.ReadLine();
			Console.WriteLine("Enter the Address");
			add=Console.ReadLine();
			 var cust = new Customer(cid,first,last,age,add);
			 EmployeeObjList.Add(cust);
		}
		
		
		
		  Console.WriteLine("after adding to list..");
            Console.WriteLine("Total count {0}", EmployeeObjList.Count);
            Console.WriteLine("----------------");
            foreach (var customer in EmployeeObjList)
            {
                Console.WriteLine(customer.FirstName + " " + customer.LastName+ " " + customer.CustomerId + " " + customer.Age + " " + customer.Address);
            }
			
			label1: string res=option();
	       switch(res)
		     {
			   case "1":
			   {
				 Console.WriteLine("Enter the customer id to be Deleted:");
			 string find=Console.ReadLine();
			
			 foreach (var customer in EmployeeObjList)
            {
               if(customer.CustomerId==find)
			   {
				var del=customer;  
				 EmployeeObjList.Remove(del);
			   }
			   else
			   {
				   Console.WriteLine("Record doesn't exist for this ID"); 
			   }
            }
			
			Console.WriteLine("Total count {0}", EmployeeObjList.Count);
            Console.WriteLine("----------------");
			foreach (var customer in EmployeeObjList)
            {
                Console.WriteLine(customer.FirstName + " " + customer.LastName+ " " + customer.CustomerId + " " + customer.Age + " " + customer.Address);
            }  
			   }
			    goto label1;
			  //break;
			case "2":
			{
				Console.WriteLine("Enter the CustomerId of the customer whose details has to be searched:");
			 string search=Console.ReadLine();
			
			 foreach (var customer in EmployeeObjList)
            {
               if(customer.CustomerId==search)
			   {
				var sea=customer;  
				 EmployeeObjList.Contains(sea);
				 Console.WriteLine(customer.FirstName + " " + customer.LastName+ " " + customer.CustomerId + " " + customer.Age + " " + customer.Address);
			   }
			   else
			   {
				 Console.WriteLine("Record doesn't exist for this ID");  
				 
			   }
            }
			}
			goto label1;
			//break;
			case "3":
			{
			Console.WriteLine("Do you want to add more items:");
			int n1=Convert.ToInt32(Console.ReadLine());
			
			for(int i=1;i<=n1;i++)
		{
			Console.WriteLine("Enter the detail {0}",i);
			Console.WriteLine("Enter the Customer Id");
			cid=Console.ReadLine();
			Console.WriteLine("Enter the First Name");
			first=Console.ReadLine();
			Console.WriteLine("Enter the Last Name");
			last=Console.ReadLine();
			Console.WriteLine("Enter the Age");
			age=Console.ReadLine();
			Console.WriteLine("Enter the Address");
			add=Console.ReadLine();
			 var cust = new Customer(cid,first,last,age,add);
			 EmployeeObjList.Add(cust);
		}
		
		Console.WriteLine("Total count {0}", EmployeeObjList.Count);
            Console.WriteLine("----------------");
			foreach (var customer in EmployeeObjList)
            {
                Console.WriteLine(customer.FirstName + " " + customer.LastName+ " " + customer.CustomerId + " " + customer.Age + " " + customer.Address);
            }
			}
			goto label1;
			//break;
		
			case "4":
			Environment.Exit(0);
			 //EmployeeObjList.Sort();
             ////foreach (var customer in EmployeeObjList)
           // {
                //Console.WriteLine(customer.FirstName + " " + customer.LastName+ " " + customer.CustomerId + " " + customer.Age + " " + customer.Address);
           // }

       
			break;
			default:
			Console.WriteLine("Enter the valid Values");
			break;
		}
			
			 //Console.WriteLine("after removing");
           /* businessObjList.Remove(person1);
            Console.WriteLine("Total count {0}", businessObjList.Count);
            Console.WriteLine("----------------");
*/          
			/*Console.WriteLine("Enter the customer id to be Deleted:");
			 string find=Console.ReadLine();
			
			 foreach (var customer in EmployeeObjList)
            {
               if(customer.CustomerId==find)
			   {
				var del=customer;  
				 EmployeeObjList.Remove(del);
			   }
			   else
			   {
				   Console.WriteLine("Record doesn't exist for this ID"); 
			   }
            }
			
			Console.WriteLine("Total count {0}", EmployeeObjList.Count);
            Console.WriteLine("----------------");
			foreach (var customer in EmployeeObjList)
            {
                Console.WriteLine(customer.FirstName + " " + customer.LastName+ " " + customer.CustomerId + " " + customer.Age + " " + customer.Address);
            }*/
			
			/*Console.WriteLine("Enter the CustomerId of the customer whose details has to be searched:");
			 string search=Console.ReadLine();
			
			 foreach (var customer in EmployeeObjList)
            {
               if(customer.CustomerId==search)
			   {
				var sea=customer;  
				 EmployeeObjList.Contains(sea);
				 Console.WriteLine(customer.FirstName + " " + customer.LastName+ " " + customer.CustomerId + " " + customer.Age + " " + customer.Address);
			   }
			   else
			   {
				 Console.WriteLine("Record doesn't exist for this ID");  
				 
			   }
            }*/
			/*Console.WriteLine("Do you want to add more items:");
			int n1=Convert.ToInt32(Console.ReadLine());
			
			for(int i=1;i<=n1;i++)
		{
			Console.WriteLine("Enter the detail {0}",i);
			Console.WriteLine("Enter the Customer Id");
			cid=Console.ReadLine();
			Console.WriteLine("Enter the First Name");
			first=Console.ReadLine();
			Console.WriteLine("Enter the Last Name");
			last=Console.ReadLine();
			Console.WriteLine("Enter the Age");
			age=Console.ReadLine();
			Console.WriteLine("Enter the Address");
			add=Console.ReadLine();
			 var cust = new Customer(cid,first,last,age,add);
			 EmployeeObjList.Add(cust);
		}
		
		Console.WriteLine("Total count {0}", EmployeeObjList.Count);
            Console.WriteLine("----------------");
			foreach (var customer in EmployeeObjList)
            {
                Console.WriteLine(customer.FirstName + " " + customer.LastName+ " " + customer.CustomerId + " " + customer.Age + " " + customer.Address);
            }*/
	
			/*.Sort();
			Console.WriteLine("----------------");
			foreach (var customer in EmployeeObjList)
            {
                Console.WriteLine(customer.FirstName + " " + customer.LastName+ " " + customer.CustomerId + " " + customer.Age + " " + customer.Address);
            }*/
			
			
			
           /*Console.WriteLine("Enter the customer id to be:");
		   string find=Console.ReadLine();
            businessObjList.Remove(find);
            foreach (var person in businessObjList)
            {
                Console.WriteLine(person.FirstName + " " + person.LastName);
            }

            if (businessObjList.Contains(person2))
            {
                Console.WriteLine("Search matched");
                Console.WriteLine(businessObjList[0].FirstName + "--" + businessObjList[0].LastName);
            }

            */
			
		
		
			
			
		}
		  static string option()
	  {
		  Console.WriteLine("1=>remove items");
		   Console.WriteLine("2=>Searching");
		    Console.WriteLine("3=>adding new entries");
			Console.WriteLine("4=>sorting");
			Console.WriteLine("5=>exit");
		  string op=Console.ReadLine();
		  return op;
	  }
	}
	