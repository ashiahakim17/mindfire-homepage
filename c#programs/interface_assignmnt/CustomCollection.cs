/*Task details:
-----------------------------------------------------------------

Problem #1
----------------------------------------------------------------------------------------

Create a Customer class having following properties.
1.CustomerId
2.FirstName
3.LastName
4.Age
5.Address

Create a custom generic collection class , which will be used for below operations.

1.Adding a customer to the custom generic collection.
2.Removing a customer from the custom generic collection.
3.Searching a customer in the custom generic collection.
4.Listing all the customers in the custom generic collection.
5.Sorting the customer collection based on custom order.


Create a console application that will support these above operations.
Users can input various details of a customer and can use the add,remove,search,sort and list
functionalities.



Problem #2
-----------------------------------------------------------------------------------------

Replace the custom generic collection class with List<T> and do the same operations.
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace ConsoleApplication1.GenericCollection
{
    //Abstract base class for all business object in the Business Logic Layer
    public abstract class CustomerObjectBase
    {
        protected Guid? _UniqueId;
        //local member variable which stores the object's UniqueId

        //Default constructor
        public CustomerObjectBase()
        {
            //create a new unique id for this business object
            _UniqueId = Guid.NewGuid();
        }

        //UniqueId property for every business object
        public Guid? UniqueId
        {
            get
            {
                return _UniqueId;
            }
            set
            {
                _UniqueId = value;
            }
        }
    }
public class Customer:CustomerObjectBase,IComparable<Customer>
			{
				private string _FirstName = "";
				private string _LastName = "";
				private int _CustomerId ;
				private int _Age ;
				private string _Address = "";

				//Paramaterized constructor for immediate instantiation
				public Customer(int id,string first, string last,int age,string address)
				{
					_CustomerId=id;
					_FirstName = first;
					_LastName = last;
					_Age=age;
					_Address=address;
					
				}
			public int CustomerId
					{
						get
						{
							return _CustomerId;
						}
						set
						{
							_CustomerId = value;
						}
					}
			public string FirstName
					{
						get
						{
							return _FirstName;
						}
						set
						{
							_FirstName = value;
						}
					}
			public string LastName
					{
						get
						{
							return _LastName;
						}
						set
						{
							_LastName = value;
						}
					}
			public int Age
					{
						get
						{
							return _Age;
						}
						set
						{
							_Age = value;
						}
					}
			public string Address
					{
						get
						{
							return _Address;
						}
						set
						{
							_Address = value;
						}
					}
					

				public  int CompareTo(Customer other)
				{
					// Comparing each item floating value using the following conditions
					return (this.CustomerId != other.CustomerId) ?
						((this.CustomerId < other.CustomerId) ? 1 : -1) : 0;
				}
			}
			
    public class CustomerObjectCollection<T> : ICollection<T> where T : CustomerObjectBase
    {
        //inner ArrayList object
        protected ArrayList _innerArray;
        //flag for setting collection to read-only
        //mode (not used in this example)
        protected bool _IsReadOnly;

        // Default constructor
        public CustomerObjectCollection()
        {
            _innerArray = new ArrayList();
        }

        // Default accessor for the collection 
        public T this[int index]
        {
            get
            {
                return (T)_innerArray[index];
            }
            set
            {
                _innerArray[index] = value;
            }
        }

        // Number of elements in the collection
        public virtual int Count
        {
            get
            {
                return _innerArray.Count;
            }
        }

        // Flag sets whether or not this collection is read-only
        public virtual bool IsReadOnly
        {
            get
            {
                return _IsReadOnly;
            }
        }

        // Add a business object to the collection
        public virtual void Add(T CustomerObject)
        {
            _innerArray.Add(CustomerObject);
        }
		 public void Sort()
					{
						// Invoking orderby LINQ aggregation method to sort
						// the items stored in the array m_Items
						for (int i = 0; i < _innerArray.Count; i++)
            {
				 T obj = (T)_innerArray[i];
				  //compare the CustomerObjectBase UniqueId property
                if (obj.UniqueId == CustomerObject.UniqueId)
                {
                    //remove item from inner ArrayList at index i
                    _innerArray.RemoveAt(i);
                    result = true;
                    break;
                }
				 
				
			}
							
					}

        // Remove first instance of a business object from the collection 
        public virtual bool Remove(T CustomerObject)
        {
            bool result = false;

            //loop through the inner array's indices
            for (int i = 0; i < _innerArray.Count; i++)
            {
                //store current index being checked
                T obj = (T)_innerArray[i];

                //compare the CustomerObjectBase UniqueId property
                if (obj.UniqueId == CustomerObject.UniqueId)
                {
                    //remove item from inner ArrayList at index i
                    _innerArray.RemoveAt(i);
                    result = true;
                    break;
                }
            }

            return result;
        }

        // Returns true/false based on whether or not it finds
        // the requested object in the collection.
        public bool Contains(T CustomerObject)
        {
            //loop through the inner ArrayList
            foreach (T obj in _innerArray)
            {
                //compare the CustomerObjectBase UniqueId property
                if (obj.UniqueId == CustomerObject.UniqueId)
                {
                    //if it matches return true
                    return true;
                }
            }
            //no match
            return false;
        }

        // Copy objects from this collection into another array
        public virtual void CopyTo(T[] CustomerObjectArray, int index)
        {
            throw new Exception(
              "This Method is not valid for this implementation.");
        }

        // Clear the collection of all it's elements
        public virtual void Clear()
        {
            _innerArray.Clear();
        }

        // Returns custom generic enumerator for this CustomerObjectCollection
        public virtual IEnumerator<T> GetEnumerator()
        {
            //return a custom enumerator object instantiated
            //to use this CustomerObjectCollection 
            return new CustomerObjectEnumerator<T>(this);
        }

        // Explicit non-generic interface implementation for IEnumerable
        // extended and required by ICollection (implemented by ICollection<T>)
		 IEnumerator IEnumerable.GetEnumerator()
        {
            return new CustomerObjectEnumerator<T>(this);
        }
      // public virtual IComparer<T> GetComparer()
      //  {
            //return a custom comparer object instantiated
            //to use this CustomerObjectCollection 
           // return new CustomerObjectComparer<T>(this);
       // }
	  // IComparer IComparable.GetComparer()
//{
           // return new CustomerObjectComparer<T>(this);
        //public void Sort()
			//	{
				//		Customer.Sort(new Comparer_byAge());
//}
		}
	

        // Explicit non-generic interface implementation for IComparable
        // extended and required by ICollection (implemented by ICollection<T>)
       
    

    public class CustomerObjectEnumerator<T> : IEnumerator<T> where T : CustomerObjectBase 
    {
        protected CustomerObjectCollection<T> _collection; //enumerated collection
        protected int index; //current index
        protected T _current; //current enumerated object in the collection

        // Default constructor
        public CustomerObjectEnumerator()
        {
            //nothing
        }

        // Paramaterized constructor which takes
        // the collection which this enumerator will enumerate
        public CustomerObjectEnumerator(CustomerObjectCollection<T> collection)
        {
            _collection = collection;
            index = -1;
            _current = default(T);
        }

        // Current Enumerated object in the inner collection
        public virtual T Current
        {
            get
            {
                return _current;
            }
        }

        // Explicit non-generic interface implementation for IEnumerator
        // (extended and required by IEnumerator<T>)
        object IEnumerator.Current
        {
            get
            {
                return _current;
            }
        }

        // Dispose method
        public virtual void Dispose()
        {
            _collection = null;
            _current = default(T);
            index = -1;
        }

        // Move to next element in the inner collection
        public virtual bool MoveNext()
        {
            //make sure we are within the bounds of the collection
            if (++index >= _collection.Count)
            {
                //if not return false
                return false;
            }
            else
            {
                //if we are, then set the current element
                //to the next object in the collection
                _current = _collection[index];
            }
            //return true
            return true;
        }

        // Reset the enumerator
        public virtual void Reset()
        {
            _current = default(T); //reset current object
            index = -1;
        }
    }
	 public class CustomerComparer : IComparer<Customer> 
    {
        public int Compare(Customer x,Customer y)
        {
            // Invoking a CompareTo method to compare each 
            // item x of the collection with the current item y specified
            return x.CompareTo(y);
        }
    }
	

/*
   public class CustomerObjectComparer<T> : IComparer<T> where T : CustomerObjectBase 
    {
        protected CustomerObjectCollection<T> _collection; //enumerated collection
        //protected int index; //current index
       // protected T _current; //current enumerated object in the collection

        // Default constructor
        public CustomerObjectComparer()
        {
            //nothing
        }

        // Paramaterized constructor which takes
        // the collection which this enumerator will enumerate
        public CustomerObjectComparer(CustomerObjectCollection<T> collection1,CustomerObjectCollection<T> collection2)
				{
					_collection1 = collection1;
					_collection2 = collection2;
					
				}
		public int Compare(T x,T y)
				{
					if(x.CustomerId<y.CustomerId)
							return -1;
					if(x.CustomerId>y.CustomerId)
							return 1;
						return 0;
					
							
				}
		public int CompareTo(T x)
				{
					if(this.CustomerId<x.CustomerId)
							return -1;
					else if(this.CustomerId>x.CustomerId)
							return 1;
					else
							return 0;;
					
							
				}
	/*	public int CompareTo(object obj)

			{

				if (obj is Customer)

				{

					Person p2 = (Customer)obj;

					return _Age.CompareTo(p2.Age);

				}

				else

					throw new ArgumentException("Object is not a Customer.");

			}
		public int Compare(object x, object y)

        {

            Customer p1;

            Customer p2;


            if (x is Customer)

                p1 = x as Customer;

            else

                throw new ArgumentException("Object is not of type Person.");


            if (y is Customer)

                p2 = y as Customer;

            else

                throw new ArgumentException("Object is not of type Person.");


            return p1.CompareTo(p2, _comparisonType);

        }



        
    }*/

	internal class Program
    {
        private static void Main(string[] args)
        {
           
            CustomCollectionProg();

            Console.ReadKey();
            Console.Read();
        }

        private static void CustomCollectionProg()
        {
			Console.WriteLine("Enter the number of Customer");
			int n=Convert.ToInt32(Console.ReadLine());
		//	List<Customer> lstData = new List<Customer>();
			  var customerObjList = new CustomerObjectCollection<Customer>();
			for(int i=0;i<n;i++)
					{
						Console.WriteLine("Enter the CustomerId");
						int id=Convert.ToInt32(Console.ReadLine());
						Console.WriteLine("Enter the FirstName");
						string first=Console.ReadLine();
						Console.WriteLine("Enter the LastName");
						string last=Console.ReadLine();
						Console.WriteLine("Enter the Age");
						int age=Convert.ToInt32(Console.ReadLine());
						Console.WriteLine("Enter the Address");
						string address=Console.ReadLine();
					 var Cus = new Customer(id,first,last,age,address);	
					   customerObjList.Add(Cus);
					}
                 
          
          

            Console.WriteLine("after adding to list..");
            Console.WriteLine("Total count {0}", customerObjList.Count);
            Console.WriteLine("----------------");
            foreach (var customer in customerObjList)
            {
                Console.WriteLine(customer.CustomerId+ " " +customer.FirstName + " " + customer.LastName+ " " + customer.Age+ " " + customer.Address);
            }
			//Sort
			//foreach (var customer in customerObjList)
//{
            //   int  id=customer.CustomerId;
//Console.WriteLine(id);
           // }
			customerObjList.Sort();
			 foreach (var customer in customerObjList)
            {
                Console.WriteLine(customer.CustomerId+ " " +customer.FirstName + " " + customer.LastName+ " " + customer.Age+ " " + customer.Address);
            }
			//Remove
            Console.WriteLine("Enter the id of the person whose details is to be removed");
			int cusid=Convert.ToInt32(Console.ReadLine());
			foreach (var customer in customerObjList)
					{
						if(customer.CustomerId==cusid)
						{
							var elem=customer;
							customerObjList.Remove(elem);
						}
						else
						{
							Console.WriteLine("Customer doesn't exist ");
						}
					}
            Console.WriteLine("after removing");
            
            Console.WriteLine("Total count {0}", customerObjList.Count);
            Console.WriteLine("----------------");

            foreach (var customer in customerObjList)
            {
                 Console.WriteLine(customer.CustomerId+ " " +customer.FirstName + " " + customer.LastName+ " " + customer.Age+ " " + customer.Address);
            }
			//Search
			Console.WriteLine("Enter the id of the person whose details is to be searched");
			int cusidsearch=Convert.ToInt32(Console.ReadLine());
			foreach (var customer in customerObjList)
					{
							var elem=customer;
							//customerObjList.Remove(elem);
						 if (customerObjList.Contains(elem))
								{
									Console.WriteLine("Search matched");
									Console.WriteLine(customerObjList[0].CustomerId + "--" + customerObjList[0].FirstName + "--" + customerObjList[0].LastName + "--" + 
														customerObjList[0].Age + "--" + customerObjList[0].Address );
								}
						else
								{
									Console.WriteLine("Customer doesn't exist ");
								}
					}
           
        }
}}
	
	


		