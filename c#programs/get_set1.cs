using System;
class getset1
{
	string name;
	int num;
	public string Name
	{
		get
		{
			return name;
		}
		set
		{
			name=value;
		}
	}
	public int Num
	{
		get
		{
			return num;
		}
		set
		{
			num=value*value;
		}
	}
	
}
class getset
{
	
	public static void Main()
	{
		getset1 gs=new getset1();
		gs.Name="Ashia";
		Console.WriteLine("Name is {0}",gs.Name);
		Console.WriteLine("Enter the numeric value");
		int a=Convert.ToInt32(Console.ReadLine());
		gs.Num=a;
		Console.WriteLine("square is {0}",gs.Num);
		
	}
}